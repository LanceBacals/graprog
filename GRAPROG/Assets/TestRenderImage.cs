﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TestRenderImage : MonoBehaviour {

    public Shader currentShader;
    public float grayScaleAmount = 1.0f;
    private Material currentMaterial;
    public float vignetteScaleAmount = 1.0f;
    Material material
    {
        get
        {
            if (currentMaterial == null)
            {
                currentMaterial = new Material(currentShader);
                currentMaterial.hideFlags = HideFlags.HideAndDontSave;
            }
            return currentMaterial;
        }
        
    }


	// Use this for initialization
	void Start () {
        if (!SystemInfo.supportsImageEffects)
        {
            enabled = false;
            return;
        }
        if (!currentShader && !currentShader.isSupported)
        {
            enabled = false;
        }
	}

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (currentShader != null)
        {
            material.SetFloat("_VignetteAmount", vignetteScaleAmount);
            Graphics.Blit(source, destination, material);
        }
        else
        {
            Graphics.Blit(source, destination);
        }
    }

    // Update is called once per frame
    void Update () {
        grayScaleAmount = Mathf.Clamp(grayScaleAmount, 0.0f, 1.0f);
        vignetteScaleAmount = Mathf.Clamp(vignetteScaleAmount, 0.0f, 1.0f);
    }

    private void OnDisable()
    {
        if (currentMaterial)
        {
            DestroyImmediate(currentMaterial);
        }
    }
}
